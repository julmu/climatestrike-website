import {useWindowSize} from "./Hooks";
import {IDetectionService} from "../interfaces/services";
import {isNumber} from "util";


export class DetectionService implements IDetectionService {
    private static _instance = new DetectionService();

    public static instance(): IDetectionService {
        return DetectionService._instance;
    }

    public isSmall() {
        const size = useWindowSize();
        if (isNumber(size.width)) {
            return size.width < 768;
        }
        return undefined;
    }
}