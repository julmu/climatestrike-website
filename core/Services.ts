import {IServices} from "../interfaces/services";
import {ColorService} from "./ColorService";
import {HttpService} from "./HttpService";

export class Services implements IServices {
    private static _instance: IServices = new Services();

    public static instance(): IServices {
        return Services._instance;
    }

    public $color = ColorService.instance();
    public $http = HttpService.instance();
}
