import {IDataServiceOptions} from "./@def";
import {ICache} from "../core/@def";
import {Cache} from "../core/Cache";
import {IPageModelStrategy} from "../../interfaces/model";

export class DataService implements IPageModelStrategy {
    private delegate: IPageModelStrategy;
    private cache: ICache;

    constructor(options: IDataServiceOptions) {
        this.cache = new Cache(options.cache);
    }

    public setDelegate(delegate: IPageModelStrategy) {
        this.delegate = delegate;
    }

    public getPages() {
        if (!this.delegate) {
            throw new Error("DataService - No strategy set");
        }
        const provider = () => this.delegate.getPages();
        return this.cache.get("pages", provider);
    }

    public getContent(slug: string, language: string) {
        if (!this.delegate) {
            throw new Error("DataService - No strategy set");
        }
        const key = ["content", slug, language].join("-");
        const provider = () => this.delegate.getContent(slug, language);
        return this.cache.get(key, provider);
    }
}