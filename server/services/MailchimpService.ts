import {toUpper, forEach} from "lodash";


import {log} from "./Logger";
import {IMailchimpService} from "./@def";
import {HttpService} from "../../core/HttpService";
import {IDictionary} from "../../interfaces/core";



export class MailchimpService implements IMailchimpService {
    private $http = new HttpService();
    constructor(private apiKey: string, private id: string, private mapping: IDictionary<string>) {}

    public register(data: any): Promise<void> {
        const formData: any = {};
        forEach(data, (value, key) => {
            if (key === "language") {
                formData[this.mapping[key]] = toUpper(value);
            }
            else {
                formData[this.mapping[key]] = value;
            }
        });
        // logic from here: https://www.codementor.io/@mattgoldspink/integrate-mailchimp-with-nodejs-app-du10854xp
        const mailchimpInstance = "us3";
        const listUniqueId = this.id;
        const url = "https://" + mailchimpInstance + ".api.mailchimp.com/3.0/lists/" + listUniqueId + "/members/";
        return this.$http.post(
            url,
            {
                "email_address": data.email,
                "status": "pending",
                "merge_fields": formData
            },
            {
                headers: {
                    "Content-Type": "application/json;charset=utf-8",
                    "Authorization": "Basic " + Buffer.from("any:" + this.apiKey).toString("base64")
                }
            })
            .then((response) => {
                log.debug(response);
                if (response.status < 300 || (response.status === 400 && (response.data as any).body.title === "Member Exists")) {
                    log.debug("MailchimpService - Registation sucessfull");
                    return Promise.resolve();
                }
                else {
                    log.error("MailchimpService - Error during registration: " + response.data);
                }

            })
            .catch((err) => {
                if (err.response) {
                    if (err.response.status === 400 && err.response.data.title === "Member Exists")
                        return Promise.resolve();
                }
                log.debug("MailchimpService - Request failed: " + err);
                return Promise.reject(err);
            });
    }
}
