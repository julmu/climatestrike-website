import * as _ from "lodash";
import axios from "axios";

import {IDictionary} from "../../interfaces/core";
import {IWordPressAPIConfig, IWordPressAPI, IWordPressPage, IWordPressPost, IWordPressCategory} from "../../interfaces/api";



export class WordPressAPI implements IWordPressAPI {

    constructor(private config: IWordPressAPIConfig) {}

    public async getPages(): Promise<IWordPressPage[]> {
        return this.get("pages");
    }

    public async getCategories(): Promise<IWordPressCategory[]> {
        return this.get<IWordPressCategory[]>("categories");
    }

    public async getPosts(categoryId?: number): Promise<IWordPressPost[]> {
        return this.get("posts", {categories: "" + categoryId});
    }

    private async get<T>(route: string, params?: IDictionary<string>): Promise<T> {
        // FIXME - Add proper pagination. For that, inspect response header and refetch for all pages
        // See https://developer.wordpress.org/rest-api/using-the-rest-api/pagination/
        const queryParams = _.merge({per_page: "100"}, params);
        const url = new URL(route, this.config.baseUrl);
        _.forEach(queryParams, (value, key) => {
            if (value) {
                url.searchParams.set(key, value);
            }
        });
        try {
            const response = await axios.get(url.href);
            return Promise.resolve(response.data);
        }
        catch (err) {
            return Promise.reject(err);
        }
    }
}