# URL
https://www.facebook.com/api/graphql/

# Upcoming recurring events
av=0&__user=0&__a=1&__dyn=7AgNe5Gmawgrolg9odoyGzEy4QjFwn8S2Sq2i5U4e1FxebjyorxuF8vyUuKew-Dxq2WdxK4ohx3wCwxxicw8258e8hwj82oG3i0wpE9UbU8ofHG3q5U4m12wRyU-3K1tx278-0JUhwKK6Uy22225o-cBK1Uy82Yxa5bjy8aEaoGqfwl8aoy13wLwBgK4oK4UN1W2y2O0B8bUbGwCxe1LwYzUuxy4o5S1dw&__csr=&__req=4&__beoa=0&__pc=PHASED%3ADEFAULT&dpr=1&__rev=1001575705&__s=c7of0j%3Ajzcu2i%3A1zlkhr&__hsi=6777605118673116998-0&lsd=AVoor7j6&jazoest=2702&__spin_r=1001575705&__spin_b=trunk&__spin_t=1578034162&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=PageEventsTabRecurringEventsCardRendererQuery&variables=%7B%22pageID%22%3A%221227679007379847%22%7D&doc_id=1798378126951231

# Past events
av=0&__user=0&__a=1&__dyn=7AgNe5Gmawgrolg9odoyGzEy4QjFwn8S2Sq2i5U4e1FxebjyorxuF8vyUuKew-Dxq2WdxK4ohx3wCwxxicw8258e8hwj82oG3i0wpE9UbU8ofHG3q5U4m12wRyU-3K1tx278-0JUhwKK6Uy22225o-cBK1Uy82Yxa5bjy8aEaoGqfwl8aoy13wLwBgK4oK4UN1W2y2O0B8bUbGwCxe1LwYzUuxy4o5S1dw&__csr=&__req=5&__beoa=0&__pc=PHASED%3ADEFAULT&dpr=1&__rev=1001575705&__s=c7of0j%3Ajzcu2i%3A1zlkhr&__hsi=6777605118673116998-0&lsd=AVoor7j6&jazoest=2702&__spin_r=1001575705&__spin_b=trunk&__spin_t=1578034162&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=PageEventsTabPastEventsCardRendererQuery&variables=%7B%22pageID%22%3A%221227679007379847%22%7D&doc_id=2620926351263365

# Upcoming events
av=0&__user=0&__a=1&__dyn=7AgNe5Gmawgrolg9odoyGzEy4QjFwn8S2Sq2i5U4e1FxebjyorxuF8vyUuKew-Dxq2WdxK4ohx3wCwxxicw8258e8hwj82oG3i0wpE9UbU8ofHG3q5U4m12wRyU-3K1tx278-0JUhwKK6Uy22225o-cBK1Uy82Yxa5bjy8aEaoGqfwl8aoy13wLwBgK4oK4UN1W2y2O0B8bUbGwCxe1LwYzUuxy4o5S1dw&__csr=&__req=6&__beoa=0&__pc=PHASED%3ADEFAULT&dpr=1&__rev=1001575705&__s=c7of0j%3Ajzcu2i%3A1zlkhr&__hsi=6777605118673116998-0&lsd=AVoor7j6&jazoest=2702&__spin_r=1001575705&__spin_b=trunk&__spin_t=1578034162&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=PageEventsTabUpcomingEventsCardRendererQuery&variables=%7B%22pageID%22%3A%221227679007379847%22%7D&doc_id=2455863461165494

# Event text
av=0&__user=0&__a=1&__dyn=7xe6Fo4OQ5E5ObGexOnFwn84afxi5U4e1FxebzEdEc8uxa0z8S2S4okwhEkwaa7oqx60Vo1eEfFE4W0y8460GogxO0SobEa8465o-cw5MKi8wnU4m1owsU9k2C220jG2W3S0H8-7Eow&__csr=&__req=3&__beoa=0&__pc=PHASED%3ADEFAULT&dpr=1&__rev=1001575764&__s=ciuna3%3A9kfx3z%3Ax46omi&__hsi=6777612188812803553-0&lsd=AVoor7j6&jazoest=2702&__spin_r=1001575764&__spin_b=trunk&__spin_t=1578035808&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=EventsEventDetailsCardRendererQuery&variables=%7B%22eventID%22%3A%22459885948293758%22%7D&doc_id=1640160956043533
