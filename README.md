# General information
This project contains the code for the new climatestrike.ch website, implementing the design proposed [here](https://www.figma.com/proto/gS764DdLo3IkDJgHfni6fd/Climatestrike-Website-new-Prototype?node-id=260%3A0&viewport=299%2C362%2C0.019355816766619682&scaling=scale-down).

# Getting started
```
git clone https://gitlab.com/omg_me/climatestrike-website.git
cd climatestrike-website
npm i
```

If you want to use `strapi` as CMS, additionally run the following commands:
```
cd ./server/strapi
npm i
```

## Development
For development, run:
```
npm run dev
```
in the root folder.

If you want to use `strapi` as CMS, additionally run the following commands:
```
cd ./server/strapi
npm run develop
```

Then open
```
localhost:3000
```
in your browser.

### Configuration
There is a configuration file, located at `./config/config.ts`. All of the configuration should be self-explainatory. If you want to use the mock API, change `datasources.strategy` from `strapi` to `mock`. You can then modify `server/api/MockAPI.ts` and restart the server for the changes to take effect.

## Production
For a production install, use
```
npm run build
# Temporary solution to keep the process running in the background
NODE_ENV=production nohup npm run dev > /dev/null &
```

## Troubleshooting
On Linux, if you get an error like `Watchpack Error (watcher): Error: ENOSPC: System limit for number of file watchers reached, watch <some-dir>`, you can increase your file watchers limit using

```
sudo sysctl fs.inotify.max_user_watches=<some-number>
sudo sysctl -p
```
or to make it permanent
```
echo fs.inotify.max_user_watches=<some-number> | sudo tee -a /etc/sysctl.conf
sudo sysctl -p
```

You can print the current limit with `cat /proc/sys/fs/inotify/max_user_watches`.



# Contributions
See [CONTRIBUTING](CONTRIBUTING.md).

# License
See [LICENSE](LICENSE).