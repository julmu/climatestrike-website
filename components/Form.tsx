import * as React from "react";
import {map, filter} from "lodash";

import {IFormField} from "../interfaces/core";
import {withTranslation} from "../core/I18n";
import {II18nProps, IFormProps} from "./@def";
import {Input} from "@material-ui/core";


const CustomForm: React.FC<IFormProps & II18nProps> = (props) => {
    const {t, fields, submitText} = props;
    const visibleFields = filter(fields, field => !field.hideBeforeFocus);
    const isFocused = false;
    const formFields = isFocused ? fields : visibleFields;

    return <form>
        {map(formFields, (formField: IFormField) => {
            return <Input
                type={formField.type}
                placeholder={t(formField.placeholder)}
            />;
        })}
        <Input type="submit" value={t(submitText) as string} />
    </form >;
};
// Only show error after a field is touched.
//     return <Form layout={layout} onSubmit={this.handleSubmit} >
//         {map(formFields, (field) => {
//             const error = isFocused && (getFieldError(field.key) || (field.required && !getFieldValue(field.key) && t("This field is required!")));
//             errors.push(error);
//             return <Form.Item key={field.key} validateStatus={error ? "error" : ""} help={error || ""} >
//                 {getFieldDecorator(field.key, {rules: [{type: field.pattern ? null : field.typeRule, message: t("Please enter a valid " + field.key)}, {required: field.required, message: t("This field is required!")}]})(
//                     <Input
//                         type={field.type}
//                         prefix={<Icon type={field.icon} style={{color: "rgba(0,0,0,.25)"}} />}
//                         placeholder={t(field.placeholder)}
//                     />,
//                 )}
//             </Form.Item>;
//         })}
//         <Form.Item key="submit">
//             <Button type="primary" htmlType="submit" disabled={!isFocused || some(errors)}>
//                 {t(submitText)}
//             </Button>
//         </Form.Item>
//     </Form >;
// }

export const CsForm = withTranslation()<IFormProps & II18nProps>(CustomForm);