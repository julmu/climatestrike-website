import React from "react";
import classNames from "classnames";

import {IRichTextProps, IPageContextProps} from "./@def";


export const RichText: React.FC<IRichTextProps & IPageContextProps> = ({context, html}) => {
    const className = classNames("rich-text", context.itemId);
    return <div className={className} dangerouslySetInnerHTML={{__html: html}} />;
};