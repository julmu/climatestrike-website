import React from "react";
import classNames from "classnames";
import {map} from "lodash";

import {renderLink} from "./BaseComponents";
import {IImgProps, IPageContextProps} from "./@def";


export const Img: React.FC<IImgProps & IPageContextProps> = ({image, link, context}) => {
    const className = classNames("image", context.itemId);
    const renderers = [
        () => <img key="img" src={image.src} />
    ];
    if (link) renderers.push(() => renderLink(link));
    return <div className={className} >
        {map(renderers, renderer => renderer())}
    </div>;
};