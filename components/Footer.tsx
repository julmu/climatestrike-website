import * as React from "react";
import {Grid} from "@material-ui/core";
import {map} from "lodash";

import {IFooterProps} from "./@def";

export const Footer: React.FC<IFooterProps> = ({contributions}) => {
    return <footer className="footer">
        <Grid className="footer-row" container justify="space-between" spacing={1} >
            {map(contributions, contribution => <Grid key={contribution.identifier} className={contribution.identifier} item>{contribution.provider()}</Grid>)}
        </Grid>
    </footer>;
};