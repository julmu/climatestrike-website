import React, {useRef, useEffect, useReducer} from "react";
import {includes, min, map} from "lodash";
import * as d3 from "d3";
import * as d3g from "d3-geo";
import * as topojson from "topojson";
import classNames from "classnames";

import {IMapProps, IPageContextProps, II18nProps} from "./@def";
import {withTranslation} from "../core/I18n";
import {useWindowSize} from "../core/Hooks";
import {ContentFactory} from "../factories/ContentFactory";


const reducer = (state: string[], id: string) => {
    if (includes(state, id)) {
        return [];
    }
    return [id];
};

// From https://bl.ocks.org/mbostock/4207744
const _Map: React.FC<IMapProps & IPageContextProps & II18nProps> = (props) => {
    const {context} = props;
    const d3Container = useRef(null);
    const [selection, setSelection] = useReducer(reducer, []);
    const windowSize = useWindowSize();
    const width = min([windowSize.width - 100, 960]);
    const height = width / 960 * 600;
    useEffect(
        () => {
            const svg = d3.select(d3Container.current);

            var projection = d3g.geoAlbers()
                .rotate([0, 0])
                .center([8.3, 46.8])
                .scale(16000 / 960 * width)
                .translate([width / 2, height / 2])
                .precision(.1);

            var path = d3.geoPath()
                .projection(projection);

            const swiss = props.mapData;
            var _cantons = topojson.feature(swiss, swiss.objects.cantons);
            const cantons = svg.selectAll("path")
                .data(_cantons.features);
            cantons.enter()
                .append("path")
                .classed("canton", true)
                .on("click", (d: any) => {
                    setSelection(d.id);
                });

            const svgCantons = svg.selectAll(".canton");
            svgCantons
                .attr("class", (d: any) => {
                    return classNames("canton", includes(selection, d.id) ? "selected" : null);
                });
            // The same effect can be obtained by adding a stroke to the cantons - more efficient and no trouble when rerendering (multiple boundaries)
            // svg.append("path")
            //     .datum(topojson.mesh(swiss, swiss.objects.cantons, function (a, b) {return a !== b;}))
            //     .attr("class", "canton-boundary");

            svg.selectAll("path")
                .attr("d", path);
            // svg.selectAll("text")
            //     .data(_cantons.features)
            //     .enter().append("text")
            //     .attr("transform", function (d: any) {return "translate(" + path.centroid(d) + ")";})
            //     .attr("dy", ".35em")
            //     .text(function (d: any) {return d.properties.name;});
        },
        [selection, props.mapData, width, height]
    );

    const contentFactory = new ContentFactory();
    const className = classNames("map", context.itemId);
    // prevent SSR from creating a svg with the wrong size
    if (!windowSize.width) return <div />;
    return (
        <div>
            <div key="map" className={className}>
                <svg className="map-svg"
                    width={width}
                    height={height}
                    ref={d3Container}
                />
            </div>
            <div key="contents">
                {map(props.contentItems, (item) => {
                    const config = {...item.config, selection: selection};
                    return contentFactory.create(item.type, config, props.context) as any;
                })}
            </div>
        </div>
    );
};

export const Map = withTranslation()(_Map);