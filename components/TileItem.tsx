import React from "react";
import classNames from "classnames";
import {Grid} from "@material-ui/core";

import {ITileItemProps, IPageContextProps} from "./@def";
import {renderImage, renderDescription, renderTitle, renderSubtitle} from "./BaseComponents";
import {DetectionService} from "../core/DetectionService";


export const TileItem: React.FC<ITileItemProps & IPageContextProps> = (props) => {
    const {context, title, subtitle, description, icon, image} = props;
    const className = classNames("tile-item", context.itemId);
    return <Grid className={className} container spacing={3} >
        <Grid className="tile-container" item xs={12} sm={8}>
            <Grid container spacing={3}>
                <Grid className="tile-icon" item xs={12} sm={4}>{renderImage(icon)}</Grid>
                <Grid className="tile-headers" item xs={12} sm={8}>
                    {[renderTitle(title), renderSubtitle(subtitle)]}
                </Grid>
            </Grid>
            <Grid className="tile-description" container>
                {renderDescription(description)}
            </Grid>
        </Grid>
        {!DetectionService.instance().isSmall() && <Grid className="tile-image" item sm={4}>{renderImage(image)}</Grid>}
    </Grid>;
};