import React from "react";

import {Link, withTranslation} from "../core/I18n";
import {IButtonProps, II18nProps} from "./@def";


const Button: React.FC<IButtonProps & II18nProps> = ({type, text, href, onClick, t}) => {
    const buttonText = text ? text : t(type);
    return <div className="button" onClick={() => onClick && onClick()}>
        {href && (href.indexOf("http") == 0 ?
            (<a href={href} target="_blank" rel="noopener noreferrer">{buttonText}</a>)
            : (<Link href={href}><a>{buttonText}</a></Link>))}
        {!href && <div>{buttonText}</div>}
    </div >;
};

export default withTranslation()<IButtonProps & II18nProps>(Button);