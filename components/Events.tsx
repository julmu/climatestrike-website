import React, {useState} from "react";
import classNames from "classnames";
import {Grid, Input} from "@material-ui/core";
import {first, map} from "lodash";

import {IEventsProps, IPageContextProps, II18nProps} from "./@def";
import {Selector} from "./Selector";
import {withTranslation} from "../core/I18n";
import {ContentFactory} from "../factories/ContentFactory";
import {EventDialog} from "./EventDialog";


const _Events: React.FC<IEventsProps & IPageContextProps & II18nProps> = (props) => {
    const views = ["timeline", "calendar"];
    const [view, setView] = useState(first(views));
    const [zipFilter, setZipFilter] = useState(null);
    const typeFilters = ["all", "demo", "meetups", "other"];
    const [typeFilter, setTypeFilter] = useState(first(typeFilters));
    const [dialogData, setDialogData] = useState(null);
    zipFilter;
    const {context, t} = props;
    const className = classNames(context.itemId);
    const contentFactory = new ContentFactory();
    const items = props.items;
    return <div className={className}>
        <Grid key="events-selectors" className="events-selectors" container>
            <Selector
                items={map(views, (id) => ({id, name: t(id)}))}
                selected={[view]}
                onClick={(id) => setView(id)}
                className="events-view-selector"
            />
            <Input className="zip-input" type="number" inputProps={{pattern: "[0-9]{4}", min: "1000", max: "9999"}} placeholder={t("ZIP")} onChange={(e) => {
                let value = e.target.value;
                setZipFilter(value);
            }} />
            <Selector
                items={map(typeFilters, (id) => ({id, name: t(id)}))}
                selected={[typeFilter]}
                onClick={(id) => setTypeFilter(id)}
                className="events-type-selector"
            />
        </Grid>
        <div key="event-content">
            {contentFactory.create(view, {...props, items, setDialogData}, context)}
        </div>
        {dialogData && <EventDialog dialogData={dialogData} setDialogData={setDialogData} />}
    </div >;
};

export const Events = withTranslation()(_Events);