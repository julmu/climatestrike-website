import {TObjectId, IFormField} from "../interfaces/core";
import {IPage, IContentItem} from "../interfaces/model";
import {IMenuConfig, ISubMenuConfig, IFooterConfig} from "../interfaces/config";
import {WithTranslation} from "next-i18next";


export interface IAppContext {
    activePageIdentifier: TObjectId;
    pages: IPage[];
}

export interface IContribution {
    identifier: TObjectId;
    provider: () => React.ReactNode;
}

export interface ISubMenuProps {
    className: string;
    contributions: IContribution[]
}

export interface IHeaderProps {
    useSmall: boolean;
    menuContributions: IContribution[];
    submenuContributions: IContribution[];
}

export interface IFooterProps {
    contributions: IContribution[]
}

export interface IAbstractPageProps {
    activePageIdentifier: string;
    pages: IPage[];
    items: IContentItem[];
}

export interface IPageProps {
    activePageIdentifier: TObjectId;
    pages: IPage[];
    items: IContentItem[];
}

export interface IPageWrapperProps extends IPageProps {
    title?: string;
    isMobile: boolean;
    pages: IPage[];
    menu: IMenuConfig;
    submenu: ISubMenuConfig;
    footer: IFooterConfig;
    children?: React.ReactNode;
}

export interface IPageContext {
    itemId: TObjectId;
    identifier: string;
    items: IContentItem[];
    rowItems: IContentItem[];
}

export interface IPageContextProps {
    context: IPageContext;
}

export interface II18nProps extends WithTranslation {
}

export interface IFormProps {
    submitText: string;
    submitUrl: string;
    fields: IFormField[];
}

export interface IButtonProps {
    type?: string;
    text?: string;
    href?: string;
    onClick?: () => void;
}

export interface IImageConfig {
    identifier: string;
    src: string;
    position: "background" | "left" | "right" | "alternating"; // Not yet supported
    color?: string;
    caption?: string;
}

interface ILinkConfig {
    identifier: string;
    href: string;
    type?: string;
    text?: string;
}

export interface IHomeItemProps {
    title?: string;
    subtitle?: string;
    description?: string;
    image?: IImageConfig;
    link?: ILinkConfig;
}

export interface IImgProps {
    image: IImageConfig;
    link?: ILinkConfig;
}

export interface IRichTextProps {
    html: string;
}

export interface IIframeProps {
    src: string;
}

export interface ITileItemProps {
    title: string;
    subtitle: string;
    description: string;
    icon: IImageConfig;
    image: IImageConfig;
}

export interface IEnumerationItemProps {
    title: string;
    description?: string;
    image?: IImageConfig;
    link?: ILinkConfig;
}

export interface ILinkSectionProps {
    title: string;
    description: string;
    link: ILinkConfig;
}

interface IEvent {
    description: string;
    startTime: string;
    endTime: string;
    location: string;
    zip: number;
}

export interface IEventGroup {
    title: string;
    startTime: string;
    endTime: string;
    description: string;
    categories: string[];
    events: IEvent[];
}

export interface IEventsProps {
    items: IEventGroup[];
}

export interface IEventDialogProps {
    setDialogData: (data: IEventGroup) => void;
}

export interface IMapProps {
    mapData: any; // Some freaky JSON
    contentItems: IContentItem<any>[];
}

type TContactType = "website" |
    "email" |
    "telegram" |
    "whatsapp" |
    "facebook" |
    "instagram" |
    "discord" |
    "other"

interface IContactItem {
    type: TContactType;
    link: string;
    linkName?: string;
}

export interface IContactGroup {
    identifiers: TObjectId[];
    description?: string;
    default?: boolean;
    content: IContactItem[];
}

export interface IContactDataProps {
    selection: TObjectId[];
    items: IContactGroup[];
}
