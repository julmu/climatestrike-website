import {map, sortBy, find, toLower} from "lodash";

import {TObjectId} from "../interfaces/core";
import {IWordPressAPI} from "../interfaces/api";
import {IPageModel, IPageModelStrategy, IPage, IContentItem} from "../interfaces/model";


export class PageModel implements IPageModel {
    private strategy: IPageModelStrategy;

    constructor(strategy?: IPageModelStrategy) {
        if (strategy) this.strategy = strategy;
    }

    public setStrategy(strategy: IPageModelStrategy) {
        this.strategy = strategy;
    }

    public getPages() {
        if (this.strategy) {
            return this.strategy.getPages();
        }
        throw new Error("PageModel - Missing strategy");
    }

    public getContent(slug: string, language: TObjectId) {
        if (this.strategy) {
            return this.strategy.getContent(slug, language);
        }
        throw new Error("PageModel - Missing strategy");
    }
}

export class MockModelStrategy implements IPageModelStrategy {
    constructor(private delegate: IPageModelStrategy) {}

    public async getPages() {
        return this.delegate.getPages();
    }

    public async getContent(slug: string, language: TObjectId) {
        return this.delegate.getContent(slug, language);
    }
}

export class WordPressPageModelStrategy implements IPageModelStrategy {
    constructor(private api: IWordPressAPI) {}

    public async getPages(): Promise<IPage[]> {
        try {
            const pages = await this.api.getPages();
            const sorted = sortBy(pages, (page) => page.menu_order);
            return map(sorted, (page, index) => ({id: "" + page.id, identifier: "" + page.slug, slug: page.slug, sortId: index, linkLocations: [], config: {}}));
        }
        catch (err) {
            throw new Error("WordPressPageModelStrategy - Error retrieving pages: " + err);
        }
    }

    public async getContent(slug: string): Promise<IContentItem[]> {
        try {
            const categories = await this.api.getCategories();
            const pageCategory = find(categories, (category) => toLower(category.name) === toLower(slug));
            if (pageCategory) {
                const posts = await this.api.getPosts(pageCategory.id);
                return map(posts, (post, index) => ({identifier: "" + post.id, sortId: index, type: "post", config: post.content.rendered}));
            }
            return [];
        }
        catch (err) {
            throw new Error("WordPressPageModelStrategy - Error retrieving pages: " + err);
        }
    }
}