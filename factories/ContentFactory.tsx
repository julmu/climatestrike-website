import React from "react";

import {IPageContext} from "../components/@def";
import {HomeItem} from "../components/HomeItem";
import {Iframe} from "../components/Iframe";
import {RichText} from "../components/RichText";
import {Img} from "../components/Img";
import {EnumerationItem} from "../components/EnumerationItem";
import {TileItem} from "../components/TileItem";
import {LinkSection} from "../components/LinkSection";
import {Events} from "../components/Events";
import {NoContent} from "../components/NoContent";
import {DivTimeline} from "../components/DivTimeline";
import {Calendar} from "../components/Calendar";
import {Map} from "../components/Map";
import {ContactData} from "../components/ContactData";




export class ContentFactory {
    public create(type: string, config: any, context: IPageContext): React.ReactNode {
        switch (type) {
            case "home-item":
                return <HomeItem context={context} {...config} />;
            case "iframe":
                return <Iframe context={context} {...config} />;
            case "rich-text":
                return <RichText context={context} {...config} />;
            case "img":
                return <Img context={context} {...config} />;
            case "enumeration-item":
                return <EnumerationItem context={context} {...config} />;
            case "tile-item":
                return <TileItem context={context} {...config} />;
            case "link-section":
                return <LinkSection context={context} {...config} />;
            case "events":
                return <Events context={context} {...config} />;
            case "timeline":
                return <DivTimeline context={context} {...config} />;
            case "calendar":
                return <Calendar context={context} {...config} />;
            case "map":
                return <Map context={context} {...config} />;
            case "contact-data":
                return <ContactData context={context} {...config} />;
            case "no-content-available":
                return <NoContent context={context} {...config} />;
            default:
                throw new Error(`ContentFactory - Cannot find type '${type}'`);
        }
    }
}