// - WordPress API

interface IWordPressBase {
    id: number;
    date: string;
    modified: string;
    slug: string;
    status: string;
    type: string;
    title: {
        rendered: string;
    }
}

// Most important keys for https://polylang.climate-dev.ch/wp-json/wp/v2/pages
export interface IWordPressPage extends IWordPressBase {
    menu_order: number;
}

// Most important keys for https://polylang.climate-dev.ch/wp-json/wp/v2/posts
export interface IWordPressPost extends IWordPressBase {
    content: {
        rendered: string;
    }
    excerpt: {
        rendered: string;
    }
    categories: number[];
    tags: number[];
}

// Most important keys for https://polylang.climate-dev.ch/wp-json/wp/v2/categories
export interface IWordPressCategory {
    id: number;
    name: string;
}

export interface IWordPressAPIConfig {
    baseUrl: string;
}

export interface IWordPressAPI {
    getPages(): Promise<IWordPressPage[]>;
    getCategories(): Promise<IWordPressCategory[]>;
    getPosts(categoryId?: number): Promise<IWordPressPost[]>;
}
