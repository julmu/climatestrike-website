import {AxiosResponse} from "axios";

export interface IColorService {
    getDefault(index: number): string;
    getNextDefault(): string;
    getDefaultPalette(): string[];
}

export interface IHttpService {
    get<T>(url: string): Promise<AxiosResponse<T>>;
    post<T>(url: string, data: any): Promise<AxiosResponse<T>>;
    getd<T>(url: string): Promise<T>;
    postd<T>(url: string, data: any): Promise<T>;
}

export interface IDetectionService {
    isSmall(): boolean;
}

export interface IServices {
    $color: IColorService;
    $http: IHttpService;
}