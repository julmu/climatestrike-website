import {TObjectId} from "./core";


export interface IContentItem<T = any> {
    identifier: TObjectId;
    sortId: number;
    type: string;
    config: T;
}

export enum ELinkLocation {
    header = "header",
    footer = "footer",
    footnote = "footnote"
}

interface IPageConfig {
    rowModel?: number[];
    linkLocations?: ELinkLocation[]; // e.g. ["menu", "footer"]
}

export interface IPage {
    id: TObjectId;
    identifier: string; // should differ from slug only for home page
    slug: string;
    sortId: number;
    config: IPageConfig;
}

export interface IPageModelStrategy {
    getPages(): Promise<IPage[]>;
    getContent(slug: string, language: TObjectId): Promise<IContentItem[]>;
}

export interface IPageModel extends IPageModelStrategy {
    setStrategy(strategy: IPageModelStrategy): void;
}